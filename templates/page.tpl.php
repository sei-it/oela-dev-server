<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
    <!--BEGIN HEADER -->
        <header class="row oela-header">
        
            <div id="name-and-slogan" class="col-lg-12 col-sm-12 col-xs-12">
                <?php if ($site_name || $site_slogan): ?>
                    <div class="col-lg-4 col-sm-6 hidden-xs logo">
                        <?php if($logo): ?>
                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                                <img src="<?php print $logo; ?>" alt="Department of Education Logo" class="img-responsive" />
                            </a>
                        <?php endif; ?>
                        <?php if($site_name): ?>
                            <div class="logo-text">
                                <h1><?php print $site_name; ?></h1>
                                <?php if($site_slogan): ?>
                                    <p><?php print $site_slogan; ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div> <!-- /#name-and-slogan -->
                <?php endif; ?>
                
                <?php print render($page['header']); ?>
                
            </div> 
              
              <?php print render($page['main_navigation']); ?>
            
        </header>
    <!--END HEADER -->
    
   
   <!--BEGIN CONTENT WRAPPER-->
  <div id="oela-wrap">
  	<div class="row">
        <div class="oela-content col-lg-12 col-xs-12">
        
        	<?php 
				if($page['banner']):
					print render($page['banner']);
				endif;
			?>
            
            <?php 
				if($page['page_heading']):
					print render($page['page_heading']);
				endif; 
			?>
            
            <?php 
				if($page['sidebar_first']):
					print render($page['sidebar_first']);
				endif; 
			?>
            

                <section class="internal-page-head">
                    <?php print render($title_prefix); ?>
                    <?php if ($title): ?>
                        <h2><?php print($title); ?></h2>
                    <?php endif; ?>
                    <?php print render($title_suffix); ?>
                </section>


            <section class=" search-head section-pad">

                <p>Put search stuff here</p>

            </section>

            <section class="bg-blue section-pad">
                <div class="container">

                    <p>Instructional Text Lorem ipsum dolor sit amet, ut integer lobortis, gravida rutrum. Eget donec amet
                        turpis vivamus, donec varius, nullam cursus. Et nunc lectus a odio tortor urna. Suspendisse integer
                        et sed potenti tincidunt nisl, sodales consequat wisi ipsum elit orci. Metus cras euismod dui vehicula.</p>
                </div>
            </section>

            <section class="section-pad">
                <div class="container">
                    <?php //kpr($page); ?>

                    <?php if ($tabs): ?>
                        <div class="tabs">
                            <?php print render($tabs); ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($page['highlighted']): ?>
                        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
                    <?php endif; ?>

                    <?php if ($breadcrumb): ?>
                        <div id="breadcrumb"><?php print $breadcrumb; ?></div>
                    <?php endif; ?>

                    <?php print render($page['help']); ?>

                    <a id="main-content"></a>


                    <?php if ($action_links): ?>
                        <ul class="action-links">
                            <?php print render($action_links); ?>
                        </ul>
                    <?php endif; ?>

                    <?php print $feed_icons; ?>

                    <?php print $messages; ?>


                    <?php print render($page['content']); ?>
                </div>
            </section>

            <section class="bg-slate section-pad">
                <div class="container">

                    <p>Lorem ipsum dolor sit amet, ut in lacus eros id vestibulum, felis tristique. Sed tellus mattis. Lacus dictum wisi eget, venenatis consequat elit nunc in mus praesent, sed fermentum. Vel commodo sed nulla fringilla, ut dui torquent. Pellentesque in quis ultrices turpis mi, neque nascetur, sed sit wisi morbi commodo sit mauris, odio mauris ullamcorper gravida orci montes. Penatibus auctor, amet parturient massa consectetuer et, et elit metus massa tincidunt, tortor aliquam dapibus et est, pulvinar tempor wisi donec. Rutrum netus mi libero, orci habitasse et consectetuer placerat. Sed non urna metus in nunc, posuere morbi ut nisl. In neque ipsum, netus enim eleifend ornare sed, in cursus dui viverra curae massa, commodo wisi mollis mollis et.</p>

                    <p>
                        Dui at velit vitae lectus sed. Fusce fusce molestie. Elit ornare amet a suspendisse vehicula, erat fermentum nulla erat elit, lectus luctus nunc mi pellentesque, metus volutpat sed ligula cras donec. Nibh augue natoque, non iaculis pede egestas in pharetra massa, dictumst accumsan omnis arcu, et lorem platea vel sed a ligula. Cum class, nunc dapibus sit ipsa praesent vulputate. Aliquam hendrerit nulla mauris mauris tortor, bibendum eget dignissim, arcu integer non suscipit molestie in, rhoncus viverra aliquam, varius ut habitant turpis velit mattis. Vivamus ut pellentesque. At cras eleifend aliquam convallis pellentesque in, metus nullam sit donec, a mi sapien eros pede cursus diam, in a felis. Nunc eu rhoncus viverra et donec, tristique fames velit. Justo eleifend quis nulla amet, vivamus bibendum accumsan sed risus, sed est felis lectus sem nam ante. Gravida orci pellentesque dignissim suscipit quam, diam justo orci iaculis, quis velit imperdiet accumsan, nibh elit, sem volutpat non.
                    </p>

                </div>

            </section>


            <?php
            if($page['sidebar_second']):
                print render($page['sidebar_second']);
            endif;
            ?>

            <?php
            if($page['featured_first']):
                print render($page['featured_first']);
            endif;
            ?>

            <?php
            if($page['featured_second']):
                print render($page['featured_second']);
            endif;
            ?>

            <?php
            if($page['featured_third']):
                print render($page['featured_third']);
            endif;
            ?>
            
            
        </div>
    </div>
  </div>
  <!--END CONTENT WRAPPER -->
  
  
  <!--BEGIN FOOTER -->
  <footer class="row oela-footer">
    
     <?php print render($page['footer']); ?>

      <div class="container">
          <img src="<?php print drupal_get_path('theme', 'oela2017');?>/images/ed-logo-black.png" alt="Department of Education Logo" class="img-responsive" />
      </div>
     
  </footer>
    <!--END FOOTER -->