<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
 <!--[if lt IE 7]>      
 	<html class="no-js lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>> 
<![endif]-->
<!--[if IE 7]>         
	<html class="no-js lt-ie9 lt-ie8" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>> 
<![endif]-->
<!--[if IE 8]>         
	<html class="no-js lt-ie9" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>> 
<![endif]-->

<!--[if gt IE 8]><!-->
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<!--<![endif]-->

<head profile="<?php print $grddl_profile; ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>  id="oela-body">
<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>



<!-- LOGIN MODAL -->
<div class="modal fade in" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title" id="modal-login-label">Oela Login</h3>
            </div>
            <div class="modal-body">
                <form method="post" action="user" id="user-login" accept-charset="UTF-8">
                    <div class="hiddenFields">
                        <input type="hidden" name="form_id" value="user_login">
                    </div>
                    <div class="full-width form-row">
                        Enter your username and password to log in:
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="edit-name">Username</label>
                        <input type="text" name="name" placeholder="Username..." class="form-username form-control" id="edit-name" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="edit-pass">Password</label>
                        <input type="password" name="pass" placeholder="Password..." class="form-password form-control" id="edit-pass" required>
                    </div>
                    <div class="full-width form-row">
                        <a href="/user/password" >Forgot your password?</a>
                    </div>
                    <div class="full-width form-row">
                        <button name="op" type="submit" class="oela-btn inline">Sign In</button>
                        <button type="reset" class="oela-btn inline" data-dismiss="modal">Cancel</button>
                    </div>
                    <!--                        <div class="full-width form-row">
<a href="#" class="oela-btn no-margin" onclick="javascript: $('#pnlNewAccount').show();">Click here to create a new account</a>
</div>-->
                </form>

                <!--PANEL FORGOT-->
                <div id="pnlForgot" style="display:none;">
                    <h2>Request A Password Reset Link</h2>
                    <form id="forgot_password_form" method="post" action="">
                        <div class="hiddenFields">
                            <input type="hidden" name="XID" value="">
                            <input type="hidden" name="ACT" value="">
                            <input type="hidden" name="RET" value="">
                            <input type="hidden" name="params_id" value="">
                            <input type="hidden" name="site_id" value="">
                        </div>
                        <!--<h3>Email Address</h3>-->
                        <div class="full-width form-row">
                            <input name="email" type="text" id="email" placeholder="Email Address..." required="" class="form-control">
                        </div>
                        <div class="full-width form-row">
                            <input type="submit" name="btnSendResetLink" value="Send A Reset Link By Email" class="oela-btn">
                        </div>
                        <div class="full-width form-row">
                            <div id="forgot-message-success" style="display:none;">Message sent! Please check your inbox.</div>
                            <div id="forgot-message-error" style="display:none;">No account using that email address was found. Please check your spelling and try again.</div>
                        </div>
                    </form>
                </div>
                <!--END PANEL FORGOT-->


                <!-- BEGIN NEW ACCOUNT PANEL JUST IN CASE -->

                <div id="pnlNewAccount" style="display:none;">
                    <div class="form-group">
                        <label class="sr-only" for="form-newfirstname">First Name</label>
                        <input type="text" name="newfirstname" placeholder="First Name..." class="form-newfirstname form-control" id="form-newfirstname">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-newlastname">Last Name</label>
                        <input type="text" name="newlastname" placeholder="Last Name..." class="form-newlastname form-control" id="form-newlastname">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-newemail">Email</label>
                        <input type="text" name="newemail" placeholder="Email..." class="form-newemail form-control" id="form-newemail">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-newreenteremail">Re-enter Email</label>
                        <input type="text" name="newreenteremail" placeholder="Re-enter Email..." class="form-newreenteremail form-control" id="form-newreenteremail">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-newpassword">Password</label>
                        <input type="text" name="newpassword" placeholder="Password..." class="form-newpassword form-control" id="form-newpassword">
                    </div>
                    <div class="full-width form-row">
                        <button type="submit" class="oela-btn inline">Submit</button>
                        <button type="reset" class="oela-btn inline" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

                <!-- END NEW ACCOUNT PANEL-->
            </div>
        </div>
    </div>
</div>
<!--END LOGIN MODAL -->


<script src="<?php print drupal_get_path('theme', 'oela2017'); ?>/js/jquery-1.12.4.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php print drupal_get_path('theme', 'oela2017');?>/js/jquery-1.12.4.min.js"><\/script>')
</script>
<script src="<?php print drupal_get_path('theme', 'oela2017'); ?>/js/bootstrap.min.js"></script>

</body>
</html>
