<?php

/***********************************************************************************************************************
 * *********************************************************************************************************************
 *                                      OVERRIDE THE MAIN MENU LINKS GENERATION
 * *********************************************************************************************************************
/***********************************************************************************************************************/

function oela2017_menu_tree($variables) {
	//if(preg_match('', $variables['tree']);
  	return '<ul class="nav navbar-nav center">' . $variables['tree'] . '</ul>';
}

// Customize to include Bootstrap like navigation menus
function oela2017_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
	
  if ($element['#below']) {
	
	$element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
	$element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
	
	$sub_menu = drupal_render($element['#below']);
  }
  
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  
  //Append class='active' to the link element
  if ( $element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page()) ) {
    $element['#attributes']['class'][] = 'active';
  }
  if($element['#below']){
	  $element['#attributes']['class'][] = 'dropdown';
  }
  
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


function oela2017_image($variables) {
	
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);
  $attributes['class'] = 'img-responsive';

  foreach (array('width', 'height', 'alt', 'title') as $key) {

    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }

  return '<img' . drupal_attributes($attributes) . ' />';
}

function oela2017_preprocess_views_view(&$vars){

    //kpr($vars);

}

function oela2017_form_alter(&$form, &$form_state, $form_id) {

    if(isset($form_id) && $form_id == 'search_block_form'){

        $form['#attributes'] = array('class' => array('navbar-form', 'navbar-right'));

        if(!user_is_logged_in()){
            $form['login'] = array(
                '#type'         =>  'item',
                '#markup'       =>  "<a href=\"#\" data-toggle=\"modal\" data-target=\"#modal-login\"><span class=\"loginLabel\">" . t('OELA Login') . "</span></a>",
                '#weight'       => -1

            );
        }


        if($form['search_block_form']['#type'] == 'textfield'){
            $form['search_block_form']['#attributes']['placeholder'] = 'Search';
            $form['search_block_form']['#attributes']['class'][] = 'form-control';
            $form['search_block_form']['#size'] = 26;
            $form['actions']['submit']['#value'] = t('GO');
            $form['actions']['submit']['#attributes']['class'][] = 'go';
        }

    }

}