// JavaScript Document

(function($) {
    
	$(document).ready(function(){
		$('ul.navbar-nav ul').removeClass('nav');
		$('ul.navbar-nav ul').removeClass('navbar-nav');
		$('ul.navbar-nav ul').removeClass('center');
		$('ul.navbar-nav ul').addClass('dropdown-menu');

		$('ul.nav.navbar-nav.center').append(
			'<li class="visible-xs"><form class="navbar-form navbar-right"> <div class="form-group"> <input type="text" class="form-control" placeholder="Search"> </div> <button type="submit" class="go">Go</button> </form> </li>'
		);
	});

})(jQuery);
